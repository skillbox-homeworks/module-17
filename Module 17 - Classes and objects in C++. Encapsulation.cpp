#include <iostream>
#include <cmath>

class Vector3
{
	// Constructor
public:

	Vector3() : x(0.0), y(0.0), z(0.0)
	{}

	Vector3(double init_x, double init_y, double init_z) : x(init_x), y(init_y), z(init_z)
	{}
	// End of Constructor

private:

	double x;
	double y;
	double z;

public:

	void Print()
	{
		std::cout << "(" << x << ", " << y << ", " << z << ")";
	}

	double Length()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
};

int main()
{
	Vector3 vector(1, 0, 1);

	std::cout << "Created vector: ";

	vector.Print();

	std::cout << "\nVector length: " << vector.Length();
}